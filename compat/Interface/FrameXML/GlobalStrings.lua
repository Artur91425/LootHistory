local locale = GetLocale()

if locale == 'deDE' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "Beuteverteilungsfenster automatisch öffnen|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "Die Beuteinformation dieses Gegenstandes ist nicht mehr verfügbar.";
  LOOT_HISTORY_ALL_PASSED = "Alle passen";
  LOOT_ROLLS = "Beuteverteilung";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "Bei Aktivierung öffnet sich das Beuteverteilungsfenster automatisch und zeigt die Würfe von Gruppenmitgliedern an, wenn um beim Aufheben gebundene Gegenstände gewürfelt wird.";
  RETRIEVING_DATA = "Rufe Daten ab";
  SLASH_OPEN_LOOT_HISTORY1 = "/plündern";
  SLASH_OPEN_LOOT_HISTORY2 = "/loot";
  SLASH_OPEN_LOOT_HISTORY3 = "/Beutewürfe";
  SLASH_OPEN_LOOT_HISTORY4 = "/lootrolls";
elseif locale == 'enUS' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "Auto Open Loot Rolls Window|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "That item's loot information has expired.";
  LOOT_HISTORY_ALL_PASSED = "All Passed";
  LOOT_ROLLS = "Loot Rolls";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "When checked, the Loot Rolls window will automatically open to show you your group members' rolls when Bind on Pickup items drop.";
  RETRIEVING_DATA = "Retrieving data";
  SLASH_OPEN_LOOT_HISTORY1 = "/loot";
  SLASH_OPEN_LOOT_HISTORY2 = "/loot";
  SLASH_OPEN_LOOT_HISTORY3 = "/lootrolls";
  SLASH_OPEN_LOOT_HISTORY4 = "/lootrolls";
elseif locale == 'esES' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "Abrir automáticamente la ventana de lanzamiento de dados|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "Los detalles de botín de ese objeto han caducado.";
  LOOT_HISTORY_ALL_PASSED = "Todos han pasado";
  LOOT_ROLLS = "Lanzamiento de dados";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "Seleccionar esto hará que la ventana de lanzamiento de dados se abra automáticamente para mostrar los lanzamientos de los miembros de tu grupo cuando caigan objetos que se ligan al cogerlos.";
  RETRIEVING_DATA = "Obteniendo datos";
  SLASH_OPEN_LOOT_HISTORY1 = "/loot";
  SLASH_OPEN_LOOT_HISTORY2 = "/despojar";
  SLASH_OPEN_LOOT_HISTORY3 = "/lootrolls";
  SLASH_OPEN_LOOT_HISTORY4 = "/tiradasdados";
elseif locale == 'esMX' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "Abrir automáticamente la ventana de lanzamiento de dados|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "Los detalles de botín de ese objeto han caducado.";
  LOOT_HISTORY_ALL_PASSED = "Todos han pasado";
  LOOT_ROLLS = "Lanzamiento de dados";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "Seleccionar esto hará que la ventana de lanzamiento de dados se abra automáticamente para mostrar los lanzamientos de los miembros de tu grupo cuando caigan objetos que se ligan al cogerlos.";
  RETRIEVING_DATA = "Obteniendo datos";
  SLASH_OPEN_LOOT_HISTORY1 = "/loot";
  SLASH_OPEN_LOOT_HISTORY2 = "/despojar";
  SLASH_OPEN_LOOT_HISTORY3 = "/lootrolls";
  SLASH_OPEN_LOOT_HISTORY4 = "/tiradasdados";
elseif locale == 'frFR' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "Ouverture automatique de la fenêtre des jets de butin|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "Les informations de butin de cet objet ont expiré.";
  LOOT_HISTORY_ALL_PASSED = "Tous ont passé";
  LOOT_ROLLS = "Jets de dés pour le butin";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "Lorsque cette option est cochée, la fenêtre des jets de butin apparaît automatiquement et affiche les jets des autres membres du groupe lorsqu’un objet lié quand ramassé est trouvé.";
  RETRIEVING_DATA = "Récupération de données";
  SLASH_OPEN_LOOT_HISTORY1 = "/loot";
  SLASH_OPEN_LOOT_HISTORY2 = "/butin";
  SLASH_OPEN_LOOT_HISTORY3 = "/lootrolls";
  SLASH_OPEN_LOOT_HISTORY4 = "/jetdebutin";
elseif locale == 'itIT' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "Apri automaticamente la finestra dei tiri del bottino|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "Le informazioni di depredazione di questo oggetto sono scadute.";
  LOOT_HISTORY_ALL_PASSED = "Tutti hanno passato.";
  LOOT_ROLLS = "Tiri per il bottino";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "Apre automaticamente la finestra dei tiri del bottino per mostrare i risultati del gruppo quando si depreda un oggetto che si vincola alla raccolta.";
  RETRIEVING_DATA = "Recupero dati";
  SLASH_OPEN_LOOT_HISTORY1 = "/loot";
  SLASH_OPEN_LOOT_HISTORY2 = "/bottino";
  SLASH_OPEN_LOOT_HISTORY3 = "/lootrolls";
  SLASH_OPEN_LOOT_HISTORY4 = "/tiribottino";
elseif locale == 'koKR' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "전리품 주사위 굴림 창 자동으로 열기|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "해당 아이템의 전리품 정보가 만료되었습니다.";
  LOOT_HISTORY_ALL_PASSED = "모두 포기";
  LOOT_ROLLS = "전리품 주사위 굴림";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "이 항목을 켜면 획득 시 귀속 아이템에 대한 파티원들의 주사위 굴림 상황을 보여주는 전리품 주사위 굴림 창을 자동으로 엽니다.";
  RETRIEVING_DATA = "데이터 전송 중";
  SLASH_OPEN_LOOT_HISTORY1 = "/loot";
  SLASH_OPEN_LOOT_HISTORY2 = "/전리품";
  SLASH_OPEN_LOOT_HISTORY3 = "/lootrolls";
  SLASH_OPEN_LOOT_HISTORY4 = "/전리품굴림";
elseif locale == 'ptBR' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "Abrir automaticamente a janela de Disputas de Saque|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "A informação de saque deste item expirou.";
  LOOT_HISTORY_ALL_PASSED = "Todos dispensaram";
  LOOT_ROLLS = "Rolamentos de Saque";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "Quando marcada, a janela de Disputas de Saque abrirá automaticamente para mostrar os resultados dos membros do seu grupo quando itens vinculados ao coletar forem saqueados.";
  RETRIEVING_DATA = "Obtendo dados";
  SLASH_OPEN_LOOT_HISTORY1 = "/saquear";
  SLASH_OPEN_LOOT_HISTORY2 = "/loot";
  SLASH_OPEN_LOOT_HISTORY3 = "/disputasdesaque";
  SLASH_OPEN_LOOT_HISTORY4 = "/lootrolls";
elseif locale == 'ruRU' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "Автоматическое открытие окна розыгрыша добычи|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "Информация о розыгрыше этого предмета уже устарела.";
  LOOT_HISTORY_ALL_PASSED = "Все отказались";
  LOOT_ROLLS = "Розыгрыши добычи";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "Окно розыгрыша добычи с отчетом о бросках костей будет автоматически открываться, когда в добыче окажется предмет, становящийся персональным при поднятии.";
  RETRIEVING_DATA = "Получение данных";
  SLASH_OPEN_LOOT_HISTORY1 = "/loot";
  SLASH_OPEN_LOOT_HISTORY2 = "/добыча"
  SLASH_OPEN_LOOT_HISTORY3 = "/lootrolls"
  SLASH_OPEN_LOOT_HISTORY4 = "/доббросок"
elseif locale == 'zhCN' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "自动打开战利品掷骰窗口|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "物品的拾取信息已过期。";
  LOOT_HISTORY_ALL_PASSED = "全部放弃";
  LOOT_ROLLS = "战利品掷骰";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "钩选此项后，在掉落拾取绑定物品时，将自动开启战利品掷骰窗口，向你展示你的队伍成员的掷骰情况。";
  RETRIEVING_DATA = "获取数据";
  SLASH_OPEN_LOOT_HISTORY1 = "/拾取";
  SLASH_OPEN_LOOT_HISTORY2 = "/loot";
  SLASH_OPEN_LOOT_HISTORY3 = "/战利品掷骰";
  SLASH_OPEN_LOOT_HISTORY4 = "/lootrolls";
elseif locale == 'zhTW' then
  AUTO_OPEN_LOOT_HISTORY_TEXT = "自動打開拾取記錄視窗|TInterface\\OptionsFrame\\UI-OptionsFrame-NewFeatureIcon:0:0:0:-1|t";
  ERR_LOOT_HISTORY_EXPIRED = "該物品的拾取記錄已經過期。";
  LOOT_HISTORY_ALL_PASSED = "所有人都放棄";
  LOOT_ROLLS = "拾取記錄";
  OPTION_TOOLTIP_AUTO_OPEN_LOOT_HISTORY = "勾選此選項後，每當有拾取後綁定的物品掉落時，拾取記錄視窗將會自動開啟，顯示隊伍成員的擲骰記錄。";
  RETRIEVING_DATA = "資料讀取中";
  SLASH_OPEN_LOOT_HISTORY1 = "/loot";
  SLASH_OPEN_LOOT_HISTORY2 = "/拾取";
  SLASH_OPEN_LOOT_HISTORY3 = "/lootrolls";
  SLASH_OPEN_LOOT_HISTORY4 = "/拾取記錄";
end

