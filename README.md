## Chat commands:
* /loot, /lootrolls (and more specific commans for you gameclient locale) - opens the loot history window.

## Installation
1. Download **[Latest Version](https://gitlab.com/Artur91425/LootHistory/-/archive/master/LootHistory-master.zip)**
2. Unpack the Zip file
3. Rename the folder "LootHistory-master" to "LootHistory"
4. Copy "LootHistory" into Wow-Directory\Interface\AddOns
5. Restart Wow

## Screenshots
<img src="https://gitlab.com/Artur91425/LootHistory/uploads/22c942395f8633e9a4c3db28573db7fc/WoWScrnShot_041623_153549.jpg" width="48%">
<img src="https://gitlab.com/Artur91425/LootHistory/uploads/f01aebf17c957971544b1efd9f85af84/WoWScrnShot_041623_153132.jpg" width="48%">
<img src="https://gitlab.com/Artur91425/LootHistory/uploads/70761bf402ecc8bba13e10d91d24e02f/WoWScrnShot_041623_152756.jpg" width="48%">
